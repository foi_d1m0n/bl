//
//  AbstractUser.h
//  bl
//
//  Created by Dmitry Simkin on 9/19/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AbstractUser <NSObject>

@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *nickname;

@end