//
//  Shortcuts.h
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#ifndef bl_Shortcuts_h
#define bl_Shortcuts_h

#import "AppDelegate.h"
#import "APIManager.h"
#import "MappingManager.h"
#import "DescriptorsManager.h"
#import "Storage.h"

#define Application                 [UIApplication sharedApplication]
#define ApplicationDelegate         (AppDelegate *)[[UIApplication sharedApplication] delegate]
#define SharedAPIManager                [APIManager sharedAPIManager]
#define SharedMappingManager            [MappingManager sharedMappingManager]
#define SharedDescriptorsManager        [DescriptorsManager sharedDescriptorsManager]
#define DefaultStorage                  [Storage defaultStorage]
#define UserDefaults                    [NSUserDefaults standardUserDefaults]

// frame helping shortcuts
#define ViewWidth(v)                        v.frame.size.width
#define ViewHeight(v)                       v.frame.size.height
#define ViewX(v)                            v.frame.origin.x
#define ViewY(v)                            v.frame.origin.y
#define SelfViewWidth                       self.view.bounds.size.width
#define SelfViewHeight                      self.view.bounds.size.height
#define RectX(f)                            f.origin.x
#define RectY(f)                            f.origin.y
#define RectWidth(f)                        f.size.width
#define RectHeight(f)                       f.size.height
#define RectSetWidth(f, w)                  CGRectMake(RectX(f), RectY(f), w, RectHeight(f))
#define RectSetHeight(f, h)                 CGRectMake(RectX(f), RectY(f), RectWidth(f), h)
#define RectSetX(f, x)                      CGRectMake(x, RectY(f), RectWidth(f), RectHeight(f))
#define RectSetY(f, y)                      CGRectMake(RectX(f), y, RectWidth(f), RectHeight(f))
#define RectSetSize(f, w, h)                CGRectMake(RectX(f), RectY(f), w, h)
#define RectSetOrigin(f, x, y)              CGRectMake(x, y, RectWidth(f), RectHeight(f))

#endif
