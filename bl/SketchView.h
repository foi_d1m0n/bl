//
//  SketchView.h
//  bl
//
//  Created by Dmitry Simkin on 9/25/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@protocol SketchViewDelegate <NSObject>

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;

@end

@interface SketchView : GLKView

@property (nonatomic, weak) id <SketchViewDelegate> sketchDelegate;

@end
