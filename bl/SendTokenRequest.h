//
//  SendTokenRequest.h
//  bl
//
//  Created by Dmitry Simkin on 9/20/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SendTokenRequest : NSObject

@property (nonatomic, strong) NSString *notificationToken;
@end
