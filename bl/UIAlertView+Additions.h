//
//  UIAlertView+Additions.h
//  bl
//
//  Created by Dmitry Simkin on 9/19/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Additions)

+ (UIAlertView *)showAlertWithTitle:(NSString *)title message:(NSString *)message;

@end
