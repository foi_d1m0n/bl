//
//  APIManager.m
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "APIManager.h"

#import "NetworkConstants.h"

#import "LoginRequest.h"
#import "SendTokenRequest.h"
#import "SendMessageRequest.h"
#import "ChangeNicknameRequest.h"
#import "SyncContactsRequest.h"

@interface APIManager ()

@property (nonatomic, strong) RKObjectManager *objectManager;

@end

@implementation APIManager

static NSString * const kAuthTokenHeaderKey = @"X-API-TOKEN";

+ (APIManager *)sharedAPIManager
{
    static APIManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[APIManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self configureObjectManager];
    }
    return self;
}

- (void)reset
{
    [self configureObjectManager];
}

- (void)configureObjectManager
{
    NSURL *baseURL = [NSURL URLWithString:kBaseURL];
    self.objectManager = [RKObjectManager managerWithBaseURL:baseURL];
    self.objectManager.managedObjectStore = DefaultStorage.managedObjectStore;
    [self.objectManager.HTTPClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSString *authToken = [UserDefaults valueForKey:kUserDefaultsAuthTokenKey];
    if ([authToken length])
        [self setAuthToken:authToken];
    self.objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    [RKObjectManager setSharedManager:self.objectManager];
    
    [self addDescriptors:[DescriptorsManager allDescriptors]];
    [self configureRestKitLog];
}

- (void)configureRestKitLog
{
    // HINT: RK available log components
    
//    RKLogConfigureByName("*", RKLogLevelOff);
//    RKLogConfigureByName("*", RKLogLevelTrace); // set all logs to trace,
//    RKLogConfigureByName("App", RKLogLevelTrace);
//    RKLogConfigureByName("RestKit", RKLogLevelTrace);
//    RKLogConfigureByName("RestKit/CoreData", RKLogLevelTrace);
//    RKLogConfigureByName("RestKit/CoreData/Cache", RKLogLevelTrace);
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
//    RKLogConfigureByName("RestKit/Network/CoreData", RKLogLevelTrace);
//    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
//    RKLogConfigureByName("RestKit/Search", RKLogLevelTrace);
//    RKLogConfigureByName("RestKit/Support", RKLogLevelTrace);
//    RKLogConfigureByName("RestKit/UI", RKLogLevelTrace)
}

- (void)addDescriptors:(NSArray *)descriptors
{
    for (id descriptor in descriptors)
    {
        if ([descriptor isMemberOfClass:[RKRequestDescriptor class]])
            [self.objectManager addRequestDescriptor:descriptor];
        else if ([descriptor isMemberOfClass:[RKResponseDescriptor class]])
            [self.objectManager addResponseDescriptor:descriptor];
    }
}

- (void)setAuthToken:(NSString *)token
{
    [self.objectManager.HTTPClient setDefaultHeader:kAuthTokenHeaderKey value:token];
}

#pragma mark - API

- (void)loginWithPhoneNumber:(NSString *)phoneNumber
                    password:(NSString *)password
                 withSuccess:(LoadSuccessHandler)successHandler
                     failure:(LoadFailureHandler)failureHandler;
{
    NSAssert(phoneNumber, @"Phone number is required");
    NSAssert(password, @"Password is required");
    LoginRequest *object = [LoginRequest new];
    object.phoneNumber = phoneNumber;
    object.password = password;
    [self.objectManager postObject:object
                              path:kPathPatternLogin
                        parameters:nil
                           success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                               if (successHandler)
                                   successHandler(mappingResult.firstObject);
                           }
                           failure:^(RKObjectRequestOperation *operation, NSError *error) {
                               // get error
                               if (failureHandler)
                                   failureHandler(error);
                           }];
}

//- (void)sendPushNotificationsToken:(NSData *)token
- (void)sendPushNotificationsToken:(NSString *)token
                       withSuccess:(LoadSuccessHandler)successHandler
                           failure:(LoadFailureHandler)failureHandler
{
    NSAssert(token, @"Token is required");
    User *user = [ApplicationDelegate currentUser];
    NSAssert(user, @"Cant get current user");
    SendTokenRequest *object = [SendTokenRequest new];
    object.notificationToken = token;
    [self.objectManager putObject:object path:kPathPatternSendToken parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        if (successHandler)
            successHandler(nil);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"failed to send token");
        if (failureHandler)
            failureHandler(error);
    }];
}

- (void)sendMessage:(NSString *)message
        withSuccess:(LoadSuccessHandler)successHandler
            failure:(LoadFailureHandler)failureHandler
{
    SendMessageRequest *object = [SendMessageRequest new];
    object.message = message;
    [self.objectManager postObject:object path:kPathPatternSendMessage parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        if (successHandler)
            successHandler(nil);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (failureHandler)
            failureHandler(error);        
    }];
}

- (void)changeNickname:(NSString *)nickname
           withSuccess:(LoadSuccessHandler)successHandler
               failure:(LoadFailureHandler)failureHandler
{
    NSAssert(nickname, @"Nickname is requiered");
    ChangeNicknameRequest *object = [ChangeNicknameRequest new];
    object.nickname = nickname;
    [self.objectManager putObject:object path:kPathPatternChangeNickname parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        if (successHandler)
            successHandler(nickname);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (failureHandler)
            failureHandler(error);
    }];
}

- (void)syncContacts:(NSArray *)contacts
         withSuccess:(LoadSuccessHandler)successHandler
             failure:(LoadFailureHandler)failureHandler
{
    SyncContactsRequest *object = [SyncContactsRequest new];
    [self.objectManager postObject:object path:kPathPatternSyncContacts parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        if (successHandler)
            successHandler(mappingResult.array);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        if (failureHandler)
            failureHandler(error);
    }];
}

@end
