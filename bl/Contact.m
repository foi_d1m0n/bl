//
//  Contact.m
//  bl
//
//  Created by Dmitry Simkin on 9/23/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "Contact.h"


@implementation Contact

@dynamic nickname;
@dynamic phoneNumber;

@end
