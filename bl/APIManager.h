//
//  APIManager.h
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIManager : NSObject

+ (APIManager *)sharedAPIManager;

- (void)reset;

- (void)setAuthToken:(NSString *)token;

- (void)loginWithPhoneNumber:(NSString *)phoneNumber
                    password:(NSString *)password
                 withSuccess:(LoadSuccessHandler)successHandler
                     failure:(LoadFailureHandler)failureHandler;

//- (void)sendPushNotificationsToken:(NSData *)token
- (void)sendPushNotificationsToken:(NSString *)token
                       withSuccess:(LoadSuccessHandler)successHandler
                           failure:(LoadFailureHandler)failureHandler;

- (void)sendMessage:(NSString *)message
        withSuccess:(LoadSuccessHandler)successHandler
            failure:(LoadFailureHandler)failureHandler;

- (void)changeNickname:(NSString *)nickname
           withSuccess:(LoadSuccessHandler)successHandler
               failure:(LoadFailureHandler)failureHandler;

- (void)syncContacts:(NSArray *)contacts
         withSuccess:(LoadSuccessHandler)successHandler
             failure:(LoadFailureHandler)failureHandler;

@end
