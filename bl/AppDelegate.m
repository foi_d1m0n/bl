//
//  AppDelegate.m
//  bl
//
//  Created by Dmitry Simkin on 9/17/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "AppDelegate.h"

#import "User.h"
#import "CurrentUser.h"
#import "LoginResponse.h"

#import "MainViewController.h"
#import "PhoneNumberViewController.h"
#import "ChangeNicknameViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
//    if (user is logined)
    SharedAPIManager;
//    [self registerForPushNotifications];
//    if (self.isAuthorized)
//    {
//        if (![self.currentUser.nickname length])
//        {
//            [self showChangeNicknameScreen];
//        }
//        else
//        {
//            [self showMain];
//        }
//    }
    return YES;
}

- (User *)currentUser
{
    CurrentUser *currentUser = [DefaultStorage currentUser];
    User *user = nil;
    if (currentUser)
    {
        user =[[User alloc] init];
        user.phoneNumber = currentUser.phoneNumber;
        user.nickname = currentUser.nickname;
    }
    return user;
}

- (BOOL)isAuthorized
{
    User *user = self.currentUser;
    NSString *authToken = [UserDefaults valueForKey:kUserDefaultsAuthTokenKey];
    return (user && [authToken length]);
}

- (void)didLoginWithResponse:(LoginResponse *)response
{
    User *user = [[User alloc] init];
    user.nickname = response.nickname;
    user.phoneNumber = response.phoneNumber;
    [SharedAPIManager setAuthToken:response.token];
    [UserDefaults setObject:response.token forKey:kUserDefaultsAuthTokenKey];
    [UserDefaults synchronize];
    [DefaultStorage setUserAsCurrent:user];
    if (![user.nickname length])
    {
        [self showChangeNicknameScreen];
    }
    else
    {
        [self showMain];
    }
}

- (void)showMain
{
    MainViewController *controller = [[UIStoryboard storyboardWithName:@"iPhone" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:NSStringFromClass([MainViewController class])];
    self.window.rootViewController = controller;
}

- (void)showInitialScreen
{
    UIViewController *controller = [[UIStoryboard storyboardWithName:@"iPhone" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
    self.window.rootViewController = controller;
}

- (void)showChangeNicknameScreen
{
    ChangeNicknameViewController *controller = [[UIStoryboard storyboardWithName:@"iPhone" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:NSStringFromClass([ChangeNicknameViewController class])];
    controller.completionHandler = ^(NSString *nickname, NSError *error) {
        User *user = self.currentUser;
        user.nickname = nickname;
        [DefaultStorage setUserAsCurrent:user];
        [self showMain];
    };
    self.window.rootViewController = controller;
}

- (void)logout
{
    [UserDefaults removeObjectForKey:kUserDefaultsAuthTokenKey];
    [UserDefaults removeObjectForKey:kUserDefaultsPushNotificationsTokenKey];
    
    [DefaultStorage clear];
    [SharedAPIManager reset];
    
    [self showInitialScreen];
}

#pragma makr - Push Notifications

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSData *previousRegisteredToken = [UserDefaults valueForKey:kUserDefaultsPushNotificationsTokenKey];
    NSString *tokenString = [deviceToken base64EncodedStringWithOptions:0];
    
    if (![previousRegisteredToken isEqualToData:deviceToken] && self.currentUser)
    {
        [SharedAPIManager sendPushNotificationsToken:tokenString withSuccess:^(id object) {
            [UserDefaults setObject:deviceToken forKey:kUserDefaultsPushNotificationsTokenKey];
            [UserDefaults synchronize];
            // do nothing
        } failure:^(NSError *error) {
            NSLog(@"Failed to send token");
            // retry to send later
        }];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"did fail to register for remote notif");
}

- (void)registerForPushNotifications
{
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1)
    {
        [Application registerForRemoteNotifications];
    }
    else
    {
        [Application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSString *message = [userInfo valueForKeyPath:@"aps.alert"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
    NSLog(@"Did receide remote notification");
}

@end
