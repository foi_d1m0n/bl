//
//  SyncContactsRequest.h
//  bl
//
//  Created by Dmitry Simkin on 9/23/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SyncContactsRequest : NSObject

@property (nonatomic, strong) NSArray *contacts;

@end
