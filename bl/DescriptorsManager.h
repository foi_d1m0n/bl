//
//  DescriptorsManager.h
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DescriptorsManager : NSObject

+ (DescriptorsManager *)sharedDescriptorsManager;

+ (NSArray *)allDescriptors;

+ (RKRequestDescriptor *)loginRequestDescriptor;
+ (RKResponseDescriptor *)loginResponseDescriptor;

+ (RKRequestDescriptor *)sendTokenRequestDescriptor;
+ (RKResponseDescriptor *)sendTokenResponseDescriptor;

+ (RKRequestDescriptor *)sendMessageRequestDescriptor;
+ (RKResponseDescriptor *)sendMessageResponseDescriptor;

+ (RKRequestDescriptor *)changeNicknameRequestDescriptor;
+ (RKResponseDescriptor *)changeNicknameResponseDescriptor;

+ (RKRequestDescriptor *)syncContactsRequestDescriptor;
+ (RKResponseDescriptor *)syncContactsResponseDescriptor;

@end
