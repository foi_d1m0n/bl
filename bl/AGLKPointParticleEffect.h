//
//  AGLKPointParticleEffect.h
//  OpenGLES_Ch8_3
//

#import <GLKit/GLKit.h>

@interface AGLKPointParticleEffect : NSObject  <GLKNamedEffect>

@property (nonatomic, assign) GLfloat elapsedSeconds;
@property (strong, nonatomic, readonly) GLKEffectPropertyTexture *texture2d0;
@property (strong, nonatomic, readonly) GLKEffectPropertyTransform *transform;

- (void)addParticleAtPosition:(GLKVector3)aPosition
   size:(float)aSize
   lifeSpanSeconds:(NSTimeInterval)aSpan
   fadeDurationSeconds:(NSTimeInterval)aDuration;

- (void)prepareToDraw;
- (void)draw;

@end
