//
//  ParticleShader.vsh
//  
//

/////////////////////////////////////////////////////////////////
// VERTEX ATTRIBUTES
/////////////////////////////////////////////////////////////////
attribute vec3 a_emissionPosition;
attribute vec2 a_size;
attribute vec2 a_emissionAndDeathTimes;

/////////////////////////////////////////////////////////////////
// UNIFORMS
/////////////////////////////////////////////////////////////////
uniform highp mat4      u_mvpMatrix;
uniform sampler2D       u_samplers2D[1];
uniform highp float     u_elapsedSeconds;

/////////////////////////////////////////////////////////////////
// Varyings
/////////////////////////////////////////////////////////////////
varying lowp float      v_particleOpacity;


void main()
{
    gl_Position = u_mvpMatrix * vec4(a_emissionPosition, 1.0);
    
    if (u_elapsedSeconds > a_emissionAndDeathTimes.y)
    {
        //keep original size
        gl_PointSize = a_size.x;
        highp float timeAfterDeth = u_elapsedSeconds - a_emissionAndDeathTimes.y;
        v_particleOpacity = 1.0 - min(1.0, timeAfterDeth / a_size.y);
    }
    else
    {
        highp float elapsedTime = u_elapsedSeconds - a_emissionAndDeathTimes.x;
        highp float lifeTime = a_emissionAndDeathTimes.y - a_emissionAndDeathTimes.x;
        gl_PointSize = a_size.x * (2.0 - (elapsedTime / lifeTime));
        v_particleOpacity = 1.0;
    }   
}
