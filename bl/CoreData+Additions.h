//
//  CoreData+Additions.h
//  justy.by
//
//  Created by Dmitry Simkin on 2/10/14.
//  Copyright (c) 2014 justy. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Additions)

+ (instancetype)insertNewInManagedObjectContext:(NSManagedObjectContext*)context;

- (void)awakeFromCreate;

- (NSString *)className;
+ (NSString *)className;

@end

@interface NSFetchRequest (Additions)

+ (NSFetchRequest*)fetchRequestWithEntityClass:(Class)entityClass;

@end

@interface NSEntityDescription (Additions)

+ (NSEntityDescription *)entityForClass:(Class)className inManagedObjectContext:(NSManagedObjectContext *)context;

@end

