//
//  SketchViewController.m
//  bl
//
//  Created by Dmitry Simkin on 9/25/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "SketchViewController.h"
#import "AGLKContext.h"
#import "AGLKPointParticleEffect.h"
#import "LineParticleEffect.h"
#import "SketchView.h"

#import "shaderUtil.h"
#import "fileUtil.h"
#import "debug.h"

@interface SketchViewController () <SketchViewDelegate>

@property (nonatomic, weak) IBOutlet SketchView *sketchView;

@property (strong, nonatomic) GLKBaseEffect *baseEffect;
@property (strong, nonatomic) AGLKPointParticleEffect *particleEffect;
@property (strong, nonatomic) LineParticleEffect *lineEffect;
@property (strong, nonatomic) GLKTextureInfo *ballParticleTexture;

@property (nonatomic) GLuint vboId;


@property (nonatomic) BOOL laoaded;

@property (nonatomic) BOOL firstTouch;
@property (nonatomic, assign) CGPoint location;
@property (nonatomic, assign) CGPoint previousLocation;

@end

@implementation SketchViewController

@synthesize location = location;
@synthesize previousLocation = previousLocation;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setup];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.baseEffect = nil;
    self.particleEffect = nil;
    self.lineEffect = nil;
}

- (void)setup
{
    self.sketchView.sketchDelegate = self;
//    self.sketchView.contentScaleFactor = [[UIScreen mainScreen] scale];
    self.sketchView.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    // Create an OpenGL ES 2.0 context and provide it to the
    // view
    self.sketchView.context = [[AGLKContext alloc]
                    initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    // Make the new context current
    [EAGLContext setCurrentContext:self.sketchView.context];
    
    // Create and configure base effect
    self.baseEffect = [[GLKBaseEffect alloc] init];
    // Configure a light
    self.baseEffect.light0.enabled = GL_TRUE;
    self.baseEffect.light0.ambientColor = GLKVector4Make(
                                                         0.9f, // Red
                                                         0.9f, // Green
                                                         0.9f, // Blue
                                                         1.0f);// Alpha
    self.baseEffect.light0.diffuseColor = GLKVector4Make(
                                                         1.0f, // Red
                                                         1.0f, // Green
                                                         1.0f, // Blue
                                                         1.0f);// Alpha
    
    // Load particle textures
    NSString *path = [[NSBundle bundleForClass:[self class]]
                      pathForResource:@"Particle" ofType:@"png"];
    NSAssert(nil != path, @"ball texture image not found");
    NSError *error = nil;
    self.ballParticleTexture = [GLKTextureLoader
                                textureWithContentsOfFile:path
                                options:nil
                                error:&error];
    
    // Create and configure particle effect
    self.particleEffect = [[AGLKPointParticleEffect alloc] init];
    self.particleEffect.texture2d0.name = self.ballParticleTexture.name;
    self.particleEffect.texture2d0.target = self.ballParticleTexture.target;
    
    self.lineEffect = [[LineParticleEffect alloc] init];
    self.lineEffect.texture2d0.name = self.ballParticleTexture.name;
    self.lineEffect.texture2d0.target = self.ballParticleTexture.target;
    
    // Set other persistent context state
    [(AGLKContext *)self.sketchView.context setClearColor:
     GLKVector4Make(0.2f, 0.2f, 0.2f, 1.0f)];
    [(AGLKContext *)self.sketchView.context enable:GL_DEPTH_TEST];
    [(AGLKContext *)self.sketchView.context enable:GL_BLEND];
    [(AGLKContext *)self.sketchView.context
     setBlendSourceFunction:GL_SRC_ALPHA
     destinationFunction:GL_ONE_MINUS_SRC_ALPHA];
    
}

/////////////////////////////////////////////////////////////////
//
- (void)update
{
    NSTimeInterval timeElapsed = self.timeSinceLastResume;
    NSLog(@"elapsedSeconds: %f", timeElapsed);
    
    self.particleEffect.elapsedSeconds = timeElapsed;
    
//    if(self.autoSpawnDelta < (timeElapsed - self.lastSpawnTime))
//    {
//        self.lastSpawnTime = timeElapsed;
//        
//        // Call a block to emit particles
//        void(^emitterBlock)() =
//        [self.emitterBlocks objectAtIndex:
//         self.currentEmitterIndex];
//        
//        emitterBlock();
//    }
}

//////////////////////////////////////////////////////////////
// Configure self.baseEffect's projection and modelview
// matrix for cinematic orbit around ship model.
- (void)preparePointOfViewWithAspectRatio:(GLfloat)aspectRatio
{
    // Do this here instead of -viewDidLoad because we don't
    // yet know aspectRatio in -viewDidLoad.
    self.baseEffect.transform.projectionMatrix =
    GLKMatrix4MakePerspective(
                              GLKMathDegreesToRadians(85.0f),// Standard field of view
                              aspectRatio,
                              0.1f,   // Don't make near plane too close
                              20.0f); // Far arbitrarily far enough to contain scene
    
    // Set initial point of view to reasonable arbitrary values
    // These values make most of the simulated rink visible
    self.baseEffect.transform.modelviewMatrix =
    GLKMatrix4MakeLookAt(
                         0.0, 0.0, 1.0,   // Eye position
                         0.0, 0.0, 0.0,   // Look-at position
                         0.0, 1.0, 0.0);  // Up direction
    
}

/////////////////////////////////////////////////////////////////
// GLKView delegate method: Called by the view controller's view
// whenever Cocoa Touch asks the view controller's view to
// draw itself. (In this case, render into a frame buffer that
// shares memory with a Core Animation Layer)
- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    // Calculate the aspect ratio for the scene and setup a
    // perspective projection
    const GLfloat  aspectRatio =
    (GLfloat)view.drawableWidth / (GLfloat)view.drawableHeight;
    
    // Clear back frame buffer colors (erase previous drawing)
    [(AGLKContext *)view.context clear:
     GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT];
    
    // Configure the point of view including animation
    [self preparePointOfViewWithAspectRatio:aspectRatio];
    
    // Set light position after change to point of view so that
    // light uses correct coordinate system.
    self.baseEffect.light0.position = GLKVector4Make(
                                                     0.4f,
                                                     0.4f,
                                                     -0.2f,
                                                     0.0f);// Directional light
    
    // Draw particles
    self.particleEffect.transform.projectionMatrix = self.baseEffect.transform.projectionMatrix;
    self.particleEffect.transform.modelviewMatrix = self.baseEffect.transform.modelviewMatrix;
    
    self.lineEffect.transform.projectionMatrix = self.baseEffect.transform.projectionMatrix;
    self.lineEffect.transform.modelviewMatrix = self.baseEffect.transform.modelviewMatrix;
    
    [self.lineEffect prepareToDraw];
    [self.lineEffect draw];

    [self.particleEffect prepareToDraw];
    [self.particleEffect draw];
    
    [self.baseEffect prepareToDraw];
    
    // ToDo: any other drawing here
    
#ifdef DEBUG
    {  // Report any errors
        GLenum error = glGetError();
        if(GL_NO_ERROR != error)
        {
            NSLog(@"GL Error: 0x%x", error);
        }
    }
#endif
}

- (void)drawLineFromPosition:(CGPoint)start toPosition:(CGPoint)end
{
    NSUInteger count, i;
    
    CGFloat scale = self.sketchView.contentScaleFactor;
    start.x *= scale;
    start.y *= scale;
    end.x *= scale;
    end.y *= scale;

    count = MAX(ceilf(sqrtf((end.x - start.x) * (end.x - start.x) + (end.y - start.y) * (end.y - start.y)) / 1), 1);
    for(i = 0; i < count; ++i) {
        CGPoint point;
        point.x = start.x + (end.x - start.x) * ((GLfloat)i / (GLfloat)count);
        point.y = start.y + (end.y - start.y) * ((GLfloat)i / (GLfloat)count);
        
        float lifeSpan = 0.2;
        CGFloat size = 30.0;
        GLKVector3 vector = [self positionVectorFromPosition:point];
        [self.particleEffect addParticleAtPosition:vector
                                              size:size
                                   lifeSpanSeconds:lifeSpan * (1 - i / count)
                               fadeDurationSeconds:0.1];
        
        [self.lineEffect addParticleAtPosition:vector size:size];
    }
}

- (GLKVector3)positionVectorFromPosition:(CGPoint)position
{
    CGFloat widthInPixels = (CGFloat)self.sketchView.drawableWidth;
    CGFloat heightInPixels = (CGFloat)self.sketchView.drawableHeight;
    
    const GLfloat aspectRatio = widthInPixels / heightInPixels;
    CGFloat widthVector = 1;
    CGFloat heightVector = widthVector / aspectRatio;
    
    CGFloat xPositionVector = (position.x * widthVector) / widthInPixels - (widthVector / 2);
    CGFloat yPositionVector = (position.y * heightVector) / heightInPixels - (heightVector / 2);

    GLKVector3 positionVector = GLKVector3Make(xPositionVector, yPositionVector, 0.0);
    return positionVector;
}

#pragma mark - SketchViewDelegate

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGRect				bounds = [self.sketchView bounds];
    UITouch*            touch = [[event touchesForView:self.sketchView] anyObject];
    self.firstTouch = YES;
    // Convert touch point from UIView referential to OpenGL one (upside-down flip)
    location = [touch locationInView:self.sketchView];
    location.y = bounds.size.height - location.y;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGRect				bounds = [self.sketchView bounds];
    UITouch*			touch = [[event touchesForView:self.sketchView] anyObject];
    
    // Convert touch point from UIView referential to OpenGL one (upside-down flip)
    if (self.firstTouch) {
        self.firstTouch = NO;
        previousLocation = [touch previousLocationInView:self.sketchView];
        previousLocation.y = bounds.size.height - previousLocation.y;
    } else {
        location = [touch locationInView:self.sketchView];
        location.y = bounds.size.height - location.y;
        previousLocation = [touch previousLocationInView:self.sketchView];
        previousLocation.y = bounds.size.height - previousLocation.y;
    }
    
    [self drawLineFromPosition:previousLocation toPosition:location];
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGRect				bounds = [self.sketchView bounds];
    UITouch*            touch = [[event touchesForView:self.sketchView] anyObject];
    if (self.firstTouch) {
        self.firstTouch = NO;
        previousLocation = [touch previousLocationInView:self.sketchView];
        previousLocation.y = bounds.size.height - previousLocation.y;
//        [self renderLineFromPoint:previousLocation toPoint:location];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

@end

