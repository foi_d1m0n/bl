//
//  ChangeNicknameViewController.h
//  bl
//
//  Created by Dmitry Simkin on 9/21/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ChangNicknameCompletion)(NSString *nickname, NSError *error);

@interface ChangeNicknameViewController : UIViewController

@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) ChangNicknameCompletion completionHandler;

@end
