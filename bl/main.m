//
//  main.m
//  bl
//
//  Created by Dmitry Simkin on 9/17/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
