//
//  ChangeNicknameViewController.m
//  bl
//
//  Created by Dmitry Simkin on 9/21/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "ChangeNicknameViewController.h"

@interface ChangeNicknameViewController ()

@property (nonatomic, weak) IBOutlet UITextField *nicknameTextField;

@end

@implementation ChangeNicknameViewController

- (IBAction)submitButtonPress:(id)sender
{
    NSString *nickname = self.nicknameTextField.text;
    [SharedAPIManager changeNickname:nickname withSuccess:^(id object) {
        if (self.completionHandler)
            self.completionHandler(nickname, nil);
    } failure:^(NSError *error) {
        if (self.completionHandler)
            self.completionHandler(nil, error);
    }];
}

@end
