//
//  Storage.h
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CurrentUser.h"
#import "User.h"

@interface Storage : NSObject

+ (Storage *)defaultStorage;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) RKManagedObjectStore *managedObjectStore;

- (void)saveContext;

- (CurrentUser *)currentUser;

- (void)setUserAsCurrent:(User *)user;

- (void)clear;

@end
