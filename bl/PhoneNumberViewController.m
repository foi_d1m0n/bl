//
//  PhoneNumberViewController.m
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "PhoneNumberViewController.h"
#import "PasswordViewController.h"

@interface PhoneNumberViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;

@end

@implementation PhoneNumberViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.phoneNumberTextField becomeFirstResponder];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EnterPassword"])
    {
        PasswordViewController *controller = segue.destinationViewController;
        controller.phoneNumber = self.phoneNumberTextField.text;
    }
}

#pragma mark - UITextFieldDelegate

// implement validation

@end
