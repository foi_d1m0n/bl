//
//  Storage.m
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "Storage.h"

@implementation Storage

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectStore = _managedObjectStore;

+ (Storage *)defaultStorage
{
    static Storage *defaultInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultInstance = [[Storage alloc] init];
    });
    return defaultInstance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        [self initializeCoreDataStack];
    }
    return self;
}

- (CurrentUser *)currentUser
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityClass:[CurrentUser class]];
    request.fetchLimit = 1;
    return [[self.managedObjectContext executeFetchRequest:request error:nil] lastObject];
}

- (void)setUserAsCurrent:(User *)user
{
    CurrentUser *currentUser = [self currentUser];
    if (currentUser)
        [self removeCurrentUser:currentUser];
    currentUser = [self.managedObjectContext insertNewObjectForEntityForName:[CurrentUser className]];
    currentUser.phoneNumber = user.phoneNumber;
    currentUser.nickname = user.nickname;
    [self saveContext];
}

- (void)removeCurrentUser:(CurrentUser *)currentUser
{
    [self.managedObjectContext deleteObject:currentUser];
}

- (void)clear
{
    [self removeCurrentUser:self.currentUser];
}

- (void)initializeCoreDataStack
{
    _managedObjectModel = [self createManagedObjectStore];
    _managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:_managedObjectModel];
    [_managedObjectStore createPersistentStoreCoordinator];
    _persistentStoreCoordinator = _managedObjectStore.persistentStoreCoordinator;
    //        NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"RKTwitter.sqlite"];
    //        NSString *seedPath = [[NSBundle mainBundle] pathForResource:@"RKSeedDatabase" ofType:@"sqlite"];
    //        NSError *error;
    //        NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:seedPath withConfiguration:nil options:nil error:&error];
    //        NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"bl.sqlite"];
    
    NSError *error = nil;
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    [_managedObjectStore createManagedObjectContexts];
    _managedObjectContext = _managedObjectStore.mainQueueManagedObjectContext;
    _managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:_managedObjectStore.persistentStoreManagedObjectContext];
    
}

- (NSManagedObjectModel *)createManagedObjectStore
{
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"bl" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    managedObjectContext = self.managedObjectStore.persistentStoreManagedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
