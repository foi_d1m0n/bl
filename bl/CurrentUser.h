//
//  CurrentUser.h
//  bl
//
//  Created by Dmitry Simkin on 9/19/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AbstractUser.h"

@interface CurrentUser : NSManagedObject <AbstractUser>

@end
