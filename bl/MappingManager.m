//
//  MappingManager.m
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "MappingManager.h"

#import "Enitites.h"
#import "LoginRequest.h"
#import "LoginResponse.h"

#import "SendMessageRequest.h"
#import "SendTokenRequest.h"
#import "ChangeNicknameRequest.h"
#import "SyncContactsRequest.h"

@implementation MappingManager

+ (MappingManager *)sharedMappingManager
{
    static MappingManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MappingManager alloc] init];
    });
    return sharedInstance;
}

+ (RKObjectMapping *)responseMapping
{
    return [RKObjectMapping mappingForClass:[NSMutableDictionary class]];
}

+ (RKObjectMapping *)loginRequestMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[LoginRequest class]];
    [mapping addAttributeMappingsFromArray:@[@"phoneNumber", @"password"]];
    return mapping;
}

+ (RKObjectMapping *)loginResponseMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[LoginResponse class]];
    [mapping addAttributeMappingsFromArray:@[@"phoneNumber", @"nickname", @"token"]];
    return mapping;
}

+ (RKObjectMapping *)contactMapping
{
    RKEntityMapping *mapping = [RKEntityMapping mappingForEntityForName:[Contact className] inManagedObjectStore:DefaultStorage.managedObjectStore];
    mapping.identificationAttributes = @[@"phoneNumber", @"nickname"];
    [mapping addAttributeMappingsFromArray:@[@"phoneNumber", @"nickname"]];
    return mapping;
}

+ (RKObjectMapping *)sendTokenRequestMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[SendTokenRequest class]];
    [mapping addAttributeMappingsFromArray:@[@"notificationToken"]];
    return mapping;
}

+ (RKObjectMapping *)sendTokenResponseMapping
{
    return [self responseMapping];
}

+ (RKObjectMapping *)sendMessageRequestMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[SendMessageRequest class]];
    [mapping addAttributeMappingsFromArray:@[@"message"]];
    return mapping;
}

+ (RKObjectMapping *)sendMessageResponseMapping
{
    return [self responseMapping];
}

+ (RKObjectMapping *)changeNicknameRequestMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[ChangeNicknameRequest class]];
    [mapping addAttributeMappingsFromArray:@[@"nickname"]];
    return mapping;
}

+ (RKObjectMapping *)changeNicknameResponseMapping
{
    return [self responseMapping];
}

+ (RKObjectMapping *)syncContactsRequestMapping
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[SyncContactsRequest class]];
//    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[NSArray class]];
    [mapping addAttributeMappingsFromArray:@[@"contacts"]];
    return mapping;
}

@end
