//
//  LoginRequest.h
//  bl
//
//  Created by Dmitry Simkin on 9/20/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginRequest : NSObject

@property (nonatomic, strong) NSString * password;
@property (nonatomic, strong) NSString * phoneNumber;

@end
