//
//  MappingManager.h
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MappingManager : NSObject

+ (MappingManager *)sharedMappingManager;

+ (RKObjectMapping *)loginRequestMapping;
+ (RKObjectMapping *)loginResponseMapping;

+ (RKObjectMapping *)contactMapping;

+ (RKObjectMapping *)sendTokenRequestMapping;
+ (RKObjectMapping *)sendTokenResponseMapping;

+ (RKObjectMapping *)sendMessageRequestMapping;
+ (RKObjectMapping *)sendMessageResponseMapping;

+ (RKObjectMapping *)changeNicknameRequestMapping;
+ (RKObjectMapping *)changeNicknameResponseMapping;

+ (RKObjectMapping *)syncContactsRequestMapping;

@end
