//
//  CoreData+Additions.m
//  justy.by
//
//  Created by Dmitry Simkin on 2/10/14.
//  Copyright (c) 2014 justy. All rights reserved.
//

#import "CoreData+Additions.h"

@implementation NSManagedObject (Additions)

+ (instancetype)insertNewInManagedObjectContext:(NSManagedObjectContext*)context
{
    NSString *className = NSStringFromClass(self);
    NSManagedObject *object = [NSEntityDescription insertNewObjectForEntityForName:className inManagedObjectContext:context];
    [object awakeFromCreate];
    return object;
}

- (void)awakeFromCreate
{
}

+ (NSString *)className
{
    return NSStringFromClass(self);
}

- (NSString *)className
{
    return [[self class] className];
}

@end

@implementation NSFetchRequest (Additions)

+ (NSFetchRequest *)fetchRequestWithEntityClass:(Class)entityClass
{
    NSString *className = NSStringFromClass(entityClass);
    return [NSFetchRequest fetchRequestWithEntityName:className];
}

@end

@implementation NSEntityDescription (Additions)

+ (NSEntityDescription *)entityForClass:(Class)entityClass inManagedObjectContext:(NSManagedObjectContext *)context
{
    NSString *className = NSStringFromClass(entityClass);
    return [NSEntityDescription entityForName:className inManagedObjectContext:context];
}

@end