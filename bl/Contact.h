//
//  Contact.h
//  bl
//
//  Created by Dmitry Simkin on 9/23/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AbstractUser.h"

@interface Contact : NSManagedObject <AbstractUser>

@end
