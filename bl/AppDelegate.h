//
//  AppDelegate.h
//  bl
//
//  Created by Dmitry Simkin on 9/17/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoginResponse, User;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, nonatomic) User *currentUser;

@property (readonly, nonatomic) BOOL isAuthorized;

- (void)logout;

- (void)registerForPushNotifications;
- (void)didLoginWithResponse:(LoginResponse *)response;


@end
