//
//  PasswordViewController.m
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "PasswordViewController.h"

#import "LoginResponse.h"

@interface PasswordViewController ()

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation PasswordViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.passwordTextField becomeFirstResponder];
}

- (IBAction)submitButtonPressed:(id)sender
{
    [SharedAPIManager loginWithPhoneNumber:self.phoneNumber password:self.passwordTextField.text withSuccess:^(LoginResponse *object) {
        [ApplicationDelegate didLoginWithResponse:object];
    } failure:^(NSError *error) {
        [UIAlertView showAlertWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Failed to login", nil)];
    }];
}

@end
