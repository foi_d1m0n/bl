//
//  NetworkConstants.h
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#ifndef bl_NetworkConstants_h
#define bl_NetworkConstants_h

static NSString * const kBaseURL = @"http://push.justy.by/api1.0/";

static NSString * const kPathPatternLogin = @"login";
static NSString * const kPathPatternSendToken = @"send-push-notification-token";
static NSString * const kPathPatternSendMessage = @"send";
static NSString * const kPathPatternChangeNickname = @"change-nickname";
static NSString * const kPathPatternSyncContacts = @"sync-contacts";

#endif
