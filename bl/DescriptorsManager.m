//
//  DescriptorsManager.m
//  bl
//
//  Created by Dmitry Simkin on 9/18/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "DescriptorsManager.h"
#import "NetworkConstants.h"
#import "LoginRequest.h"
#import "SendMessageRequest.h"
#import "SendTokenRequest.h"
#import "ChangeNicknameRequest.h"
#import "SyncContactsRequest.h"

@implementation DescriptorsManager

+ (DescriptorsManager *)sharedDescriptorsManager
{
    static DescriptorsManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DescriptorsManager alloc] init];
    });
    return sharedInstance;
}

+ (NSArray *)allDescriptors
{
    return @[[self loginRequestDescriptor],
             [self loginResponseDescriptor],
             [self sendTokenRequestDescriptor],
             [self sendTokenResponseDescriptor],
             [self sendMessageRequestDescriptor],
             [self sendMessageResponseDescriptor],
             [self changeNicknameRequestDescriptor],
             [self changeNicknameResponseDescriptor],
             [self syncContactsRequestDescriptor],
             [self syncContactsResponseDescriptor]];
             
}

+ (RKRequestDescriptor *)loginRequestDescriptor
{
    RKRequestDescriptor *descriptor = [RKRequestDescriptor requestDescriptorWithMapping:[MappingManager loginRequestMapping]
                                                                            objectClass:[LoginRequest class]
                                                                            rootKeyPath:nil
                                                                                 method:RKRequestMethodPOST];
    return descriptor;
}

+ (RKResponseDescriptor *)loginResponseDescriptor
{
    RKResponseDescriptor *descriptor = [RKResponseDescriptor responseDescriptorWithMapping:[MappingManager loginResponseMapping]
                                                                                    method:RKRequestMethodPOST
                                                                               pathPattern:kPathPatternLogin
                                                                                   keyPath:nil
                                                                               statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    return descriptor;
}

+ (RKRequestDescriptor *)sendTokenRequestDescriptor
{
    RKRequestDescriptor *descriptor = [RKRequestDescriptor requestDescriptorWithMapping:[MappingManager sendTokenRequestMapping] objectClass:[SendTokenRequest class] rootKeyPath:nil method:RKRequestMethodPUT];
    return descriptor;
}

+ (RKResponseDescriptor *)sendTokenResponseDescriptor
{
    RKResponseDescriptor *descriptor = [RKResponseDescriptor responseDescriptorWithMapping:[MappingManager sendTokenResponseMapping]
                                                                                    method:RKRequestMethodPUT
                                                                               pathPattern:kPathPatternSendToken
                                                                                   keyPath:nil
                                                                               statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    return descriptor;
}

+ (RKRequestDescriptor *)sendMessageRequestDescriptor
{
    RKRequestDescriptor *descriptor = [RKRequestDescriptor requestDescriptorWithMapping:[MappingManager sendMessageRequestMapping]
                                                                            objectClass:[SendMessageRequest class]
                                                                            rootKeyPath:nil
                                                                                 method:RKRequestMethodPOST];
    return descriptor;
}

+ (RKResponseDescriptor *)sendMessageResponseDescriptor
{
    RKResponseDescriptor *descriptor = [RKResponseDescriptor responseDescriptorWithMapping:[MappingManager sendMessageResponseMapping] method:RKRequestMethodPOST pathPattern:kPathPatternSendMessage keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    return descriptor;
}

+ (RKRequestDescriptor *)changeNicknameRequestDescriptor
{
    RKRequestDescriptor *descriptor = [RKRequestDescriptor requestDescriptorWithMapping:[MappingManager changeNicknameRequestMapping] objectClass:[ChangeNicknameRequest class] rootKeyPath:nil method:RKRequestMethodPUT];
    return descriptor;
}

+ (RKResponseDescriptor *)changeNicknameResponseDescriptor
{
    RKResponseDescriptor *descriptor = [RKResponseDescriptor responseDescriptorWithMapping:[MappingManager changeNicknameResponseMapping] method:RKRequestMethodPUT pathPattern:kPathPatternChangeNickname keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    return descriptor;
}

+ (RKRequestDescriptor *)syncContactsRequestDescriptor
{
    RKRequestDescriptor *descriptor = [RKRequestDescriptor requestDescriptorWithMapping:[MappingManager syncContactsRequestMapping]
                                                                            objectClass:[SyncContactsRequest class]
//                                                                            objectClass:[NSArray class]
//                                                                            rootKeyPath:@"contacts"
                                                                            rootKeyPath:nil
                                                                                 method:RKRequestMethodPOST];
    return descriptor;
}

+ (RKResponseDescriptor *)syncContactsResponseDescriptor
{
    RKResponseDescriptor *descriptor = [RKResponseDescriptor responseDescriptorWithMapping:[MappingManager contactMapping]
                                                                                    method:RKRequestMethodPOST
                                                                               pathPattern:kPathPatternSyncContacts
//                                                                                   keyPath:@"contacts"
                                                                                   keyPath:@"contacts"
                                                                               statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    return descriptor;
}

@end
