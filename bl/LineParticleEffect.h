//
//  LineParticleEffect.h
//  bl
//
//  Created by Dmitry Simkin on 10/2/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <GLKit/GLKit.h>

@interface LineParticleEffect : NSObject <GLKNamedEffect>

@property (strong, nonatomic, readonly) GLKEffectPropertyTexture *texture2d0;
@property (strong, nonatomic, readonly) GLKEffectPropertyTransform *transform;

- (void)addParticleAtPosition:(GLKVector3)aPosition
                         size:(float)aSize;

- (void)prepareToDraw;
- (void)draw;

@end
