//
//  MainViewController.m
//  bl
//
//  Created by Dmitry Simkin on 9/19/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "MainViewController.h"

#import <AddressBook/AddressBook.h>

@interface MainViewController ()

@property (nonatomic, weak) IBOutlet UILabel *nicknameLabel;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.nicknameLabel.text = [ApplicationDelegate currentUser].nickname;
    if (![[UserDefaults stringForKey:kUserDefaultsPushNotificationsTokenKey] length])
    {
        [ApplicationDelegate registerForPushNotifications];
    }
}

- (IBAction)sendPressed:(id)sender
{
    NSString *message = [NSString stringWithFormat:@"@%@ ЫЫЫЫ", [ApplicationDelegate currentUser].nickname];
    [SharedAPIManager sendMessage:message withSuccess:^(NSError *error) {
        //do nothing
    } failure:^(NSError *error) {
        
    }];
}

- (IBAction)logoutButtonPressed:(id)sender
{
    [ApplicationDelegate logout];
}

- (IBAction)syncContactsButtonPressed:(id)sender
{
    [SharedAPIManager syncContacts:[self addressBookContacts] withSuccess:^(NSArray *object) {
        NSLog(@"contacts: %@", object);
    } failure:^(NSError *error) {
        
    }];
}

- (NSArray *)addressBookContacts
{
    NSMutableArray *contacts = [NSMutableArray array];
    CFErrorRef *error = nil;
    
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    __block BOOL accessGranted = NO;
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        accessGranted = granted;
        dispatch_semaphore_signal(sema);
    });
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    
    if (accessGranted)
    {
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
        ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
        CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSource(addressBook, source);
        CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);

        for (int i = 0; i < nPeople; i++)
        {
            ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
            
            ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
            for (CFIndex i = 0; i < ABMultiValueGetCount(multiPhones); i++)
            {
                CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
                NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
                [contacts addObject:phoneNumber];
            }
#ifdef DEBUG
    NSLog(@"Phones are: %@", contacts);
#endif
        }
    }
    
    return contacts;
}

@end
