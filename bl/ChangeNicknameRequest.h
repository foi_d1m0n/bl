//
//  ChangeNicknameRequest.h
//  bl
//
//  Created by Dmitry Simkin on 9/21/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChangeNicknameRequest : NSObject

@property (nonatomic, strong) NSString *nickname;

@end
