//
//  LineParticleEffect.m
//  bl
//
//  Created by Dmitry Simkin on 10/2/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "LineParticleEffect.h"
#import "AGLKVertexAttribArrayBuffer.h"

// Type used to define particle attributes
typedef struct
{
    GLKVector3 position;
}
LineParticleAttributes;

enum
{
    AGLKMVPMatrix,
    AGLKPointSize,
    AGLKVertexColor,
    AGLKNumUniforms
};


/////////////////////////////////////////////////////////////////
// Attribute identifiers
typedef enum {
    LineParticlePosition = 0,
} LineParticleAttrib;

@interface LineParticleEffect ()
{
    GLuint program;
    GLint uniforms[AGLKNumUniforms];
}

@property (strong, nonatomic, readwrite) AGLKVertexAttribArrayBuffer *particleAttributeBuffer;
@property (nonatomic, assign, readonly) NSUInteger numberOfParticles;
@property (nonatomic, strong, readonly) NSMutableData *particleAttributesData;
@property (nonatomic, assign, readwrite) BOOL particleDataWasUpdated;

- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader
                 type:(GLenum)type
                 file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;

@end

@implementation LineParticleEffect

@synthesize texture2d0 = texture2d0;
@synthesize transform = transform;
@synthesize particleAttributeBuffer = particleAttributeBuffer;
@synthesize particleAttributesData = particleAttributesData;
@synthesize particleDataWasUpdated = particleDataWasUpdated;

/////////////////////////////////////////////////////////////////
// Designated initializer
- (id)init
{
    if (nil != (self = [super init]))
    {
        texture2d0 = [[GLKEffectPropertyTexture alloc] init];
        texture2d0.enabled = YES;
        texture2d0.name = 0;
        texture2d0.target = GLKTextureTarget2D;
        texture2d0.envMode = GLKTextureEnvModeReplace;
        transform = [[GLKEffectPropertyTransform alloc] init];
        particleAttributesData = [NSMutableData data];
    }
    
    return self;
}

/////////////////////////////////////////////////////////////////
//
- (LineParticleAttributes)particleAtIndex:(NSUInteger)anIndex
{
    NSParameterAssert(anIndex < self.numberOfParticles);
    
    const LineParticleAttributes *particlesPtr =
    (const LineParticleAttributes *)[self.particleAttributesData
                                     bytes];
    
    return particlesPtr[anIndex];
}


/////////////////////////////////////////////////////////////////
//
- (void)setParticle:(LineParticleAttributes)aParticle
            atIndex:(NSUInteger)anIndex
{
    NSParameterAssert(anIndex < self.numberOfParticles);
    
    LineParticleAttributes *particlesPtr =
    (LineParticleAttributes *)[self.particleAttributesData
                               mutableBytes];
    particlesPtr[anIndex] = aParticle;
    
    self.particleDataWasUpdated = YES;
}


/////////////////////////////////////////////////////////////////
//
- (void)addParticleAtPosition:(GLKVector3)aPosition
                         size:(float)aSize;
{
    LineParticleAttributes newParticle;
    newParticle.position = aPosition;
    
    [self.particleAttributesData appendBytes:&newParticle
                                      length:sizeof(newParticle)];
    self.particleDataWasUpdated = YES;
}


/////////////////////////////////////////////////////////////////
//
- (NSUInteger)numberOfParticles;
{
    return [self.particleAttributesData length] / 
    sizeof(LineParticleAttributes);
}


/////////////////////////////////////////////////////////////////
//  
- (void)prepareToDraw
{
    if(0 == program)
    {
        [self loadShaders];
    }
    
    if(0 != program)
    {
        glUseProgram(program);
        
        // Precalculate the mvpMatrix
        GLKMatrix4 modelViewProjectionMatrix = GLKMatrix4Multiply(
                                                                  self.transform.projectionMatrix,
                                                                  self.transform.modelviewMatrix);
        glUniformMatrix4fv(uniforms[AGLKMVPMatrix], 1, 0,
                           modelViewProjectionMatrix.m);
        
        glUniform1f(uniforms[AGLKPointSize], 15);
        CGColorRef color = [UIColor greenColor].CGColor;
        const CGFloat *components = CGColorGetComponents(color);
        GLfloat brushColor[4];          // brush color
        brushColor[0] = components[0];
        brushColor[1] = components[1];
        brushColor[2] = components[2];
        brushColor[3] = components[3];
        glUniform4fv(uniforms[AGLKVertexColor], 1, brushColor);
        
        if(self.particleDataWasUpdated)
        {
            if(nil == self.particleAttributeBuffer &&
               0 < [self.particleAttributesData length])
            {  // vertex attiributes haven't been sent to GPU yet
                self.particleAttributeBuffer =
                [[AGLKVertexAttribArrayBuffer alloc]
                 initWithAttribStride:sizeof(LineParticleAttributes)
                 numberOfVertices:
                 (GLsizei)([self.particleAttributesData length] /
                           sizeof(LineParticleAttributes))
                 bytes:[self.particleAttributesData bytes]
                 usage:GL_DYNAMIC_DRAW];
            }
            else
            {
                [self.particleAttributeBuffer
                 reinitWithAttribStride:
                 sizeof(LineParticleAttributes)
                 numberOfVertices:
                 (GLsizei)([self.particleAttributesData length] /
                           sizeof(LineParticleAttributes))
                 bytes:[self.particleAttributesData bytes]];
            }
            
            self.particleDataWasUpdated = NO;
        }
        
        [self.particleAttributeBuffer
         prepareToDrawWithAttrib:LineParticlePosition
         numberOfCoordinates:3
         attribOffset:
         offsetof(LineParticleAttributes, position)
         shouldEnable:YES];
        
        // Bind all of the textures to their respective units
        glActiveTexture(GL_TEXTURE0);
        if(0 != self.texture2d0.name && self.texture2d0.enabled)
        {
            glBindTexture(GL_TEXTURE_2D, self.texture2d0.name);
        }
        else
        {
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }
}

/////////////////////////////////////////////////////////////////
//
- (void)draw;
{
    glDepthMask(GL_FALSE);  // Disable depth buffer writes
    [self.particleAttributeBuffer
     drawArrayWithMode:GL_POINTS
     startVertexIndex:0
     numberOfVertices:(GLsizei)self.numberOfParticles];
    glDepthMask(GL_TRUE);  // Reenable depth buffer writes
}

#pragma mark -  OpenGL ES 2 shader compilation

/////////////////////////////////////////////////////////////////
//
- (BOOL)loadShaders
{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    // Create shader program.
    program = glCreateProgram();
    
    // Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:
                          @"point" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER
                        file:vertShaderPathname])
    {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }
    
    // Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:
                          @"point" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER
                        file:fragShaderPathname])
    {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }
    
    // Attach vertex shader to program.
    glAttachShader(program, vertShader);
    
    // Attach fragment shader to program.
    glAttachShader(program, fragShader);
    
    // Bind attribute locations.
    // This needs to be done prior to linking.
    glBindAttribLocation(program, LineParticlePosition,
                         "inVertex");
    
    // Link program.
    if (![self linkProgram:program])
    {
        NSLog(@"Failed to link program: %d", program);
        
        if (vertShader)
        {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader)
        {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (program)
        {
            glDeleteProgram(program);
            program = 0;
        }
        
        return NO;
    }
    
    // Get uniform locations.
    uniforms[AGLKMVPMatrix] = glGetUniformLocation(program,
                                                   "MVP");
    uniforms[AGLKPointSize] = glGetUniformLocation(program,
                                                    "pointSize");
    uniforms[AGLKVertexColor] = glGetUniformLocation(program,
                                                        "vertexColor");
    
    // Delete vertex and fragment shaders.
    if (vertShader)
    {
        glDetachShader(program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader)
    {
        glDetachShader(program, fragShader);
        glDeleteShader(fragShader);
    }
    
    return YES;
}


/////////////////////////////////////////////////////////////////
//
- (BOOL)compileShader:(GLuint *)shader
                 type:(GLenum)type
                 file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file
                                                  encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source)
    {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0)
    {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}


/////////////////////////////////////////////////////////////////
//
- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0)
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0)
    {
        return NO;
    }
    
    return YES;
}


/////////////////////////////////////////////////////////////////
// 
- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) 
    {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) 
    {
        return NO;
    }
    
    return YES;
}

@end
