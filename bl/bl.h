//
//  bl.h
//  bl
//
//  Created by Dmitry Simkin on 9/19/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#ifndef bl_bl_h
#define bl_bl_h

typedef void (^CompletionHandler)(NSError *error);
typedef void (^LoadSuccessHandler)(id object);
typedef void (^LoadFailureHandler)(NSError *error);

// UserDefaults
static NSString * const kUserDefaultsPushNotificationsTokenKey = @"PushNotificationsToken";
static NSString * const kUserDefaultsAuthTokenKey = @"AuthToken";

#endif
