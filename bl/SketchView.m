//
//  SketchView.m
//  bl
//
//  Created by Dmitry Simkin on 9/25/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "SketchView.h"

@interface SketchView ()

@end

@implementation SketchView

// Handles the start of a touch
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.sketchDelegate touchesBegan:touches withEvent:event];
}

// Handles the continuation of a touch.
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.sketchDelegate touchesMoved:touches withEvent:event];
}

// Handles the end of a touch event when the touch is a tap.
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.sketchDelegate touchesEnded:touches withEvent:event];
}

// Handles the end of a touch event.
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    // If appropriate, add code necessary to save the state of the application.
    // This application is not saving state.
}

@end
