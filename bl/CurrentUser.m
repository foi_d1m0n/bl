//
//  CurrentUser.m
//  bl
//
//  Created by Dmitry Simkin on 9/19/14.
//  Copyright (c) 2014 Dmitry Simkin. All rights reserved.
//

#import "CurrentUser.h"


@implementation CurrentUser

@dynamic phoneNumber;
@dynamic nickname;

@end
